import "./App.css";
import { Box } from "@mui/material";
import CssBaseline from "@mui/material/CssBaseline";
import React from "react";
import Header from "./components/Header/Header";
import OptionsTab from "./components/OptionsTab/OptionsTab";
import Container from "@mui/material/Container";
import LocationCards from "./components/LocationCards/LocationCards";
import { displayOnDesktop } from "./themes/CommonStyles";
import Footer from "./components/Footer/Footer";
import MobileFooter from "./components/Mobile/MobileFooter";
import FooterMenu from "./components/Footer/FooterMenu";

function App() {
  return (
    <React.Fragment>
      <CssBaseline />
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          height: "100vh",
        }}
      >
        <Box>
          <Header />
          <OptionsTab />
        </Box>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            flexGrow: 1,
            height: 100,
            overflowY: "scroll",
          }}
        >
          <Container maxWidth="xl" sx={{ mb: 3 }}>
            <LocationCards />
            <Box
              sx={{
                display: { xs: "flex", md: "none" },
              }}
            >
              <MobileFooter />
            </Box>
          </Container>
        </Box>
        <Box sx={{ display: { xs: "flex", md: "none" } }}>
          <FooterMenu />
        </Box>
        <Box sx={displayOnDesktop}>
          <Footer />
        </Box>
      </Box>
    </React.Fragment>
  );
}

export default App;
